# SPDX-License-Identifier: MIT
"""
setup.py
"""

import setuptools


setuptools.setup(
    name='solana-autobalance',
    version='0.0.3',
    url='https://gitlab.com/im-0/solana-autobalance',
    author='Ivan Mironov',
    author_email='mironov.ivan@gmail.com',
    license='MIT',
    license_files=(
        'LICENSE',
    ),
    description='Tool to automatically top up Solana balance',
    packages=('solana_autobalance', ),
    python_requires='~=3.7',
    entry_points={
        'console_scripts': [
            'solana-autobalance=solana_autobalance.main:cli_main',
        ],
    },
    include_package_data=True,
    data_files=[
        (
            '/usr/lib/systemd/system',
            [
                'systemd/solana-autobalance.service',
                'systemd/solana-autobalance.timer',
            ],
        ),
        (
            '/etc/solana-autobalance',
            [
                'etc/config.ini.example',
            ],
        ),
    ],
)
