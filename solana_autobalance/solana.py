# SPDX-License-Identifier: MIT
"""
Functions for interaction with Solana RPC.
"""

import configparser
import logging
import math
import shlex
import subprocess

import solana_autobalance.jpost
import solana_autobalance.telegram


# Timeout for Solana CLI command.
_CMD_TIMEOUT = 120.0  # seconds

# Number of Lamports in one Sol.
_LAMPORTS_IN_SOL = 1000000000  # lamports

# Logger.
_L = logging.getLogger(__name__)


def _get_rpc_url(url_or_moniker: str) -> str:
    """
    Get RPC URL.

    :param url_or_moniker: URL or moniker.
    :return: URL.
    """

    if url_or_moniker in ('m', 'mainnet-beta'):
        return 'https://api.mainnet-beta.solana.com'
    if url_or_moniker in ('t', 'testnet'):
        return 'https://api.testnet.solana.com'
    if url_or_moniker in ('d', 'devnet'):
        return 'https://api.devnet.solana.com'
    if url_or_moniker in ('l', 'localhost'):
        return 'http://localhost:8899'
    return url_or_moniker


def _get_balance(rpc_url: str, name: str, pubkey: str) -> float:
    balance = solana_autobalance.jpost.call(
        rpc_url,
        'getBalance',
        (pubkey, dict(commitment='finalized')),
    )['value']
    assert (not math.isinf(balance)) and (not math.isnan(balance)), \
        'Balance should be not NaN and not Inf'
    assert balance >= 0, f'Balance is {balance} lamports, which is less than zero'
    balance /= _LAMPORTS_IN_SOL
    _L.info('Balance of %s/%s: %f SOL', name, pubkey, balance)
    return balance


# pylint: disable=too-many-locals,too-many-statements
def process(configuration_name: str, configuration: configparser.SectionProxy) -> None:
    """
    Process one Solana account.

    :param configuration_name: Name of account configuration.
    :param configuration: Account configuration.
    :return: None.
    """

    _L.info('Processing %s...', configuration_name)

    rpc_url_or_moniker = configuration['url']
    pubkey_from = configuration['from']
    pubkey_to = configuration['to']
    min_transfer = configuration.getfloat('min_transfer')
    min_balance = configuration.getfloat('min_balance')
    target_balance = configuration.getfloat('target_balance')
    protected_balance = configuration.getfloat('protected_balance')
    skip_if_not_enough_funds = configuration.getboolean('skip_if_not_enough_funds')
    allow_unfunded_destination = configuration.getboolean('allow_unfunded_destination')
    command_str = configuration['command']

    assert (not math.isinf(min_transfer)) and (not math.isnan(min_transfer)), \
        'Minimal transfer should be not NaN or Inf'
    assert min_transfer > 0.0, 'Minimal transfer should be greater than zero'

    assert not math.isnan(min_balance), 'Minimal balance should be not NaN'
    assert min_balance > 0.0, 'Minimal balance should be greater than zero'

    assert not math.isnan(target_balance), 'Target balance should be not NaN'
    assert target_balance > 0.0, 'Target balance should be greater than zero'
    if not math.isinf(min_balance):
        assert target_balance > min_balance, 'Target balance should be greater than minimal balance'
        assert target_balance - min_balance >= min_transfer, \
            'Target balance minus minimal balance should be greater than ' \
            'or equal to minimal transfer'

    assert (not math.isinf(protected_balance)) and (not math.isnan(protected_balance)), \
        'Protected balance should be not NaN or Inf'
    assert protected_balance > 0.0, 'Protected balance should be greater than zero'

    assert skip_if_not_enough_funds is not None, \
        'skip_if_not_enough_funds is not set in configuration file'
    assert allow_unfunded_destination is not None, \
        'allow_unfunded_destination is not set in configuration file'

    command_parts = shlex.split(command_str, comments=False, posix=True)

    # Create RPC client.
    rpc_url = _get_rpc_url(rpc_url_or_moniker)
    _L.debug('Using RPC URL: %s', rpc_url)

    # Get balances.
    balance_from = _get_balance(rpc_url, 'from', pubkey_from)
    balance_to = _get_balance(rpc_url, 'to', pubkey_to)

    if not allow_unfunded_destination:
        assert balance_to > 0.0, 'Balance of destination is zero'

    # Check whether we need to transfer money or not.
    if balance_to >= min_balance:
        _L.info('Nothing to do, because to/%s %f SOL >= %f SOL', pubkey_to, balance_to, min_balance)
        return

    available = balance_from - protected_balance
    assert (available > 0.0) or skip_if_not_enough_funds, \
        f'No money on "from" account ({balance_from} SOL <= {protected_balance} SOL)'

    amount = min(target_balance - balance_to, available)
    assert (amount >= min_transfer) or skip_if_not_enough_funds, \
        f'Transfer amount too small ({amount} SOL < {min_transfer} SOL)'
    if amount < min_transfer:
        _L.info('Not enough funds on from/%s: %f SOL (amount %f SOL < minimal transfer %f sol)',
                pubkey_from, balance_from, amount, min_transfer)
        return

    # Do transfer.
    _L.info('Going to transfer %f SOL from/%s -> to/%s', amount, pubkey_from, pubkey_to)
    solana_autobalance.telegram.notify(
        configuration,
        f'[solana-autobalance] Going to top up {configuration_name}, balance of {pubkey_to}:'
        f' {balance_to} SOL (less than {min_balance} SOL); amount: {amount} SOL')

    command = [part.format(amount=amount) for part in command_parts]
    _L.info('Command: %r', command)
    completed = subprocess.run(
        command,
        shell=False,
        capture_output=True,
        timeout=_CMD_TIMEOUT,
        input=b'',
        check=False)
    if completed.returncode:
        _L.error('Command returned code %d, stdout:\n%r\nstderr:\n%r',
                 completed.returncode, completed.stdout, completed.stderr)
    else:
        _L.info('Command succeeded, stdout:\n%r\nstderr:\n%r',
                completed.stdout, completed.stderr)
    assert completed.returncode == 0, f'Command {command} failed'

    # Get balances again.
    _get_balance(rpc_url, 'from', pubkey_from)
    new_balance_to = _get_balance(rpc_url, 'to', pubkey_to)

    assert new_balance_to > balance_to, 'Balance not increased'
    assert (new_balance_to > min_balance) or math.isinf(min_balance), \
        'New balance not above threshold'
