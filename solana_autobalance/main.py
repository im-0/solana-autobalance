# SPDX-License-Identifier: MIT
"""
CLI entry point.
"""

import argparse
import configparser
import logging
import logging.handlers
import sys

import solana_autobalance.solana
import solana_autobalance.telegram


# Logger.
_L: logging.Logger


def _conv_log_level(level: str) -> int:
    return {
        'd': logging.DEBUG,
        'i': logging.INFO,
        'w': logging.WARNING,
        'e': logging.ERROR,
        'c': logging.CRITICAL,
    }[level[0]]


def _configure_logger(level: str = 'info', syslog: bool = False) -> None:
    handler: logging.Handler
    if syslog:
        formatter = logging.Formatter('%(message)s')
        handler = logging.handlers.SysLogHandler(
            address='/dev/log',
            facility=logging.handlers.SysLogHandler.LOG_DAEMON)
    else:
        formatter = logging.Formatter('%(asctime)s [%(levelname).1s] %(message)s')
        handler = logging.StreamHandler(sys.stderr)

    handler.setFormatter(formatter)

    root_logger = logging.getLogger()
    list(map(root_logger.removeHandler, root_logger.handlers[:]))
    list(map(root_logger.removeFilter, root_logger.filters[:]))

    root_logger.addHandler(handler)
    root_logger.setLevel(_conv_log_level(level))

    global _L  # pylint: disable=global-statement
    _L = logging.getLogger()


def cli_main() -> None:
    """
    CLI.
    """

    _configure_logger()

    arg_parser = argparse.ArgumentParser(
        description='Automatically top up Solana balance',
    )
    arg_parser.add_argument(
        '-l',
        '--log-level',
        action='store',
        default='info',
        type=str,
        choices=('debug', 'info', 'warning', 'error', 'critical'),
        required=False,
        help='Log level (default: %(default)s)',
    )
    arg_parser.add_argument(
        '-s',
        '--syslog',
        action='store_true',
        default=False,
        required=False,
        help='Write log into syslog instead of stderr',
    )
    arg_parser.add_argument(
        'configuration_file_path',
        metavar='CONFIG_PATH',
        action='store',
        help='Path to INI file',
    )
    args = arg_parser.parse_args()

    _configure_logger(args.log_level, args.syslog)
    _L.info('solana-autobalance started')

    parser = configparser.ConfigParser()
    parsed_files = parser.read(args.configuration_file_path)
    assert parsed_files == [args.configuration_file_path], \
        f'Unable to parse {args.configuration_file_path}'

    sections = parser.sections()
    sections.sort()
    for configuration_name in sections:
        configuration = parser[configuration_name]
        try:
            solana_autobalance.solana.process(configuration_name, configuration)
        except:
            solana_autobalance.telegram.notify(
                configuration,
                f'⚠️ [solana-autobalance] Failed for {configuration_name}, check log for details')
            raise

    _L.info('solana-autobalance finished')


if __name__ == '__main__':
    cli_main()
