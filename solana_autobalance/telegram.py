# SPDX-License-Identifier: MIT
"""
Functions for sending Telegram notifications.
"""

import configparser

import solana_autobalance.jpost


# Telegram API endpoint.
_TELEGRAM_API = 'https://api.telegram.org/bot{bot_token}/sendMessage'


def notify(configuration: configparser.SectionProxy, text: str) -> None:
    """
    Send Telegram notification.

    :param configuration: Configuration.
    :param text: Text.
    :return: None.
    """

    if (configuration.get('telegram_chat_id') is None) and \
            (configuration.get('telegram_bot_token') is None):
        return
    chat_id = configuration.getint('telegram_chat_id')
    bot_token = configuration['telegram_bot_token']

    # https://core.telegram.org/bots/api#sendmessage
    url = _TELEGRAM_API.format(bot_token=bot_token)
    data = {
        'chat_id': chat_id,
        'text': text,
    }
    response = solana_autobalance.jpost.post(url, data)
    assert response['ok'] is True, 'Telegram API call failed'
