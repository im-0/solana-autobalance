# SPDX-License-Identifier: MIT
"""
HTTP POST + JSON.
"""

import itertools
import json
import logging
from typing import Any, Dict

import urllib.request


# HTTP timeout.
_TIMEOUT = 10.0  # seconds

# Logger.
_L = logging.getLogger(__name__)

_rpc_call_ids = itertools.count()


def post(url: str, data: Dict[str, Any]) -> Dict[str, Any]:
    """
    Make HTTP POST request with JSON data.

    :param url: URL.
    :param data: Data dict.
    :return: Result dict.
    """

    request = urllib.request.Request(
        url,
        json.dumps(data).encode(),
        headers={'Content-Type': 'application/json'})
    with urllib.request.urlopen(request, timeout=_TIMEOUT) as response:
        _L.debug('URL: %s', url)
        _L.debug('Response status: %d', response.status)
        _L.debug('Response headers: %r', response.getheaders())
        response_data = response.read()
        _L.debug('Response data: %r', response_data)
        assert response.status == 200, 'Request failed'
    return json.loads(response_data)


def call(url: str, method: str, params: Any) -> Any:
    """
    Make JSON-RPC call.

    :param url: URL.
    :param method: Name of RPC method.
    :param params: Parameters for method call.
    :return: Result of method call.
    """

    call_id = next(_rpc_call_ids)
    data = {
        'jsonrpc': '2.0',
        'method': method,
        'params': params,
        'id': call_id,
    }

    response = post(url, data)
    response_ver = response['jsonrpc']
    response_id = response['id']
    response_error = response.get('error')

    assert response_ver == '2.0', \
        f'Wrong JSON-RPC response versions: {response_ver}'
    assert response_id == call_id, \
        f'Wrong JSON-RPC response ID: {response_id} (response) != {call_id} (call)'
    assert response_error is None, \
        f'JSON-RPC call failed with error {response_error}'

    return response['result']
